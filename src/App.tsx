import React from 'react'
import './App.css'

import Transactions from 'components/Transactions'

const App: React.FC = () => <Transactions />

export default App
