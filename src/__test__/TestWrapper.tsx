/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import * as React from 'react'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import i18n from 'i18n'
import { I18nextProvider } from 'react-i18next'

const mockStore = configureStore()

const StoreWrapper = ({ children }: any) => (
  <Provider store={mockStore({})}>
    <I18nextProvider i18n={i18n}>{children}</I18nextProvider>
  </Provider>
)

export default StoreWrapper
