import config from 'var/config'

export const get = (path: string) : Promise<Response> => (
  fetch(`${config.apiUrl}/${path}`).then((res) => res.json())
)