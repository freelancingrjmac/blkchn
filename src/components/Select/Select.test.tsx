/* eslint-disable @typescript-eslint/no-empty-function */
import React from 'react'
import { render } from '@testing-library/react'
import TestWrapper from '__test__/TestWrapper'

import Select from '.'

describe('Select', () => {
  it('renders', async () => {
    const a = render(
      <TestWrapper>
        <Select label='select' options={['a', 'b', 'c']} onChange={() => {}} />
      </TestWrapper>
    )
    expect(a).toMatchSnapshot()
  })
})
