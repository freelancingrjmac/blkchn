import React, { ChangeEvent } from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

const Select = styled.select`
  border: solid 2px rgb(240, 242, 247);
  padding: 4px 10px;
  margin: 0 10px;
  text-transform: capitalize;
`

const Option = styled.option`
  text-transform: capitalize;
`

const Label = styled.label``
interface Props {
  onChange: (event: ChangeEvent<HTMLSelectElement>) => void
  options: string[]
  label: string
}

const TransportSelect: React.FC<Props> = ({ onChange, options = [], label }) => {
  const { t } = useTranslation()
  return (
    <Label>
      {label}
      <Select onChange={onChange}>
        <Option value=''>{t('choose')}</Option>
        {options.map(transport => (
          <Option key={transport}>{transport}</Option>
        ))}
      </Select>
    </Label>
  )
}

export default TransportSelect
