/* eslint-disable @typescript-eslint/no-empty-function */
import React from 'react'
import { render } from '@testing-library/react'

import Input from '.'

describe('Input', () => {
  it('renders', async () => {
    const a = render(
      <Input onChange={() => {}} />,
    )
    expect(a).toMatchSnapshot()
  })
})
