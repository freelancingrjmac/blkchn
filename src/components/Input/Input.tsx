import React, { ChangeEvent } from 'react'
import styled from 'styled-components'

const Input = styled.input`
  border: solid 2px rgb(240, 242, 247);
  padding: 5px 10px;
  margin-left: 10px;
`

interface Props {
  onChange: (event: ChangeEvent<HTMLInputElement>) => void
}

const TransactionInput: React.FC<Props> = ({ onChange }) => (
  <Input placeholder='Search...' type='text' onChange={onChange} />
)

export default TransactionInput
