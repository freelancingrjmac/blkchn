import React, { Fragment, useState, ChangeEvent } from 'react'
import styled from 'styled-components'
import { useTranslation } from 'react-i18next'

import Amount from 'components/Amount'
import Crypto from 'components/Crypto'
import Select from 'components/Select'
import Input from 'components/Input'
import { TransactionItem, Transport } from 'abstractions/transactions'
import { sortTransactionsByField } from 'state/transactions/transaction.adapter'
import { getTransactionDate } from 'var/utils'

const Heading = styled.h2`
  margin-top: 0;
`

const TransactionsContainer = styled.div`
  padding: 20px;
`

const TransactionHeader = styled.div`
  background-color: #eee;
  border-radius: 4px;
  padding: 10px;
`

const TransactionTable = styled.div`
  display: grid;
  grid-template-columns: repeat(7, 14%);
  font-size: 13px;
  margin-top: 20px;
`
const TransactionCell = styled.div`
  border-bottom: 1px solid rgb(240, 242, 247);
  padding: 15px;
  text-overflow: ellipsis;
  overflow-x: hidden;
`

const TransactionCellHeader = styled(TransactionCell)`
  font-weight: bold;
`

interface Props {
  getTransactionsByTransport: (transport: string) => TransactionItem[]
  getTransactionsByTerm: (term: string) => TransactionItem[]
}

const Transactions: React.FC<Props> = props => {
  const { t } = useTranslation()
  const [filter, setFilter] = useState<string>('')
  const [term, setTerm] = useState<string>('')
  const [sort, setSort] = useState<string>('date')
  const { getTransactionsByTransport, getTransactionsByTerm } = props
  const transactions = term
    ? getTransactionsByTerm(term)
    : getTransactionsByTransport(filter)
  const sortedTransactions = sortTransactionsByField(sort, transactions)
  return (
    <TransactionsContainer>
      <Heading>{t('transactions')}</Heading>
      <TransactionHeader>
        <Select
          label='Filter'
          options={['Bitcoin', 'Ethereum', 'Custodial']}
          onChange={(e: ChangeEvent<{ value: string }>) => {
            console.log(e?.currentTarget?.value)
            setFilter(e?.currentTarget?.value || Transport.All)
          }}
        />
        <Select
          label='Sort'
          options={['date', 'from', 'to', 'transport', 'status']}
          onChange={(e: ChangeEvent<{ value: string }>) => {
            setSort(e?.currentTarget?.value)
          }}
        />
        <Input
          onChange={(e: ChangeEvent<{ value: string }>) => {
            setTerm(e?.currentTarget?.value)
          }}
        />
      </TransactionHeader>
      <TransactionTable>
        <TransactionCellHeader>{t('date')}</TransactionCellHeader>
        <TransactionCellHeader>{t('amount')}</TransactionCellHeader>
        <TransactionCellHeader>{t('amount-crypto')}</TransactionCellHeader>
        <TransactionCellHeader>{t('from')}</TransactionCellHeader>
        <TransactionCellHeader>{t('to')}</TransactionCellHeader>
        <TransactionCellHeader>{t('transport')}</TransactionCellHeader>
        <TransactionCellHeader>{t('status')}</TransactionCellHeader>
        {sortedTransactions.map(transaction => (
          <Fragment key={transaction.id}>
            <TransactionCell>
              {getTransactionDate(transaction.date).toDateString()}
            </TransactionCell>
            <TransactionCell>
              <Amount
                transport={transaction.transport}
                amount={transaction.amount}
                type={transaction.type}
              />
            </TransactionCell>
            <TransactionCell>
              <Crypto
                transport={transaction.transport}
                amount={transaction.amount}
                type={transaction.type}
              />
            </TransactionCell>
            <TransactionCell>{transaction.from}</TransactionCell>
            <TransactionCell>{transaction.to}</TransactionCell>
            <TransactionCell>{transaction.transport}</TransactionCell>
            <TransactionCell>{transaction.status}</TransactionCell>
          </Fragment>
        ))}
      </TransactionTable>
    </TransactionsContainer>
  )
}

export default Transactions
