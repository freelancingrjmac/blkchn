import React from 'react'
import { render } from '@testing-library/react'
import TestWrapper from '__test__/TestWrapper'

import Transactions from '.'

describe('Transactions', () => {
  it('renders', async () => {
    const a = render(
      <TestWrapper>
        <Transactions />
      </TestWrapper>,
    )
    expect(a).toMatchSnapshot()
  })
})
