import { RootState } from 'abstractions'
import { connect } from 'react-redux'
import {
  getTransactionsByTransport,
  getTransactionsByTerm,
} from 'state/transactions/transactions.selectors'

import Transactions from './Transactions'

const mapStateToProps = (state: RootState) => ({
  getTransactionsByTransport: (transport: string) => getTransactionsByTransport(transport)(state),
  getTransactionsByTerm: (term: string) => getTransactionsByTerm(term)(state),
})

export default connect(mapStateToProps)(Transactions)
