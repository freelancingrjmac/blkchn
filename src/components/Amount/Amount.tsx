import React from 'react'
import styled from 'styled-components'

import { satoshiToBtc, weiToEth } from 'var/utils'
import { Transport, TxType } from 'abstractions/transactions'
import { multiply } from 'ramda'
import { Price } from 'abstractions/price'

interface TextProps  {
  isGreen?: boolean
}

const Text = styled.span`
  color: ${(props : TextProps) => 
    props.isGreen 
      ? 'green' 
      : 'red'
  };
`

interface Props {
  amount: number | string
  transport: Transport
  type: TxType
  prices: Price,
}

const Amount: React.FC<Props> = (props): JSX.Element => {
  const { prices } = props
  const { BTC: btc, ETH: eth } = prices
  const { amount, transport, type } = props
  const isGreen = type === TxType.received  || type === TxType.buy
  switch (transport) {
    case Transport.Bitcoin:
      return (
        <Text isGreen={isGreen}>
          ${multiply(btc, satoshiToBtc(amount as number)).toFixed(2)}
        </Text>
      )
    case Transport.Ethereum:
      return (
        <Text isGreen={isGreen}>
          ${multiply(eth, weiToEth(amount as number)).toFixed(2)}
        </Text>
      )
    default:
      return <Text isGreen={isGreen}>${amount}</Text>
  }
}

export default Amount