
import { RootState } from 'abstractions'
import { connect } from 'react-redux'
import { getPrices } from 'state/prices/price.selectors'

import Amount from './Amount'

const mapStateToProps = (state: RootState) => ({
  prices: getPrices(state),
})

export default connect(mapStateToProps)(Amount)
