
import { RootState } from 'abstractions'
import { connect } from 'react-redux'
import { getPrices } from 'state/prices/price.selectors'

import Crypto from './Crypto'

const mapStateToProps = (state: RootState) => ({
  prices: getPrices(state),
})

export default connect(mapStateToProps)(Crypto)
