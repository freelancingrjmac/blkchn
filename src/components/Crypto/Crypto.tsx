
import React from 'react'
import styled from 'styled-components'

import { satoshiToBtc, weiToEth } from 'var/utils'
import { Transport, TxType } from 'abstractions/transactions'
import { Price } from 'abstractions/price'

interface TextProps  {
  isGreen?: boolean
}

const Text = styled.span`
  color: ${(props : TextProps) => 
    props.isGreen 
      ? 'green' 
      : 'red'
  };
`

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`
interface Props {
  amount: number | string
  transport: Transport
  type: TxType
  prices: Price
}
  

const Crypto: React.FC<Props> = (props): JSX.Element => {
  const { prices, amount, transport, type } = props
  const { BTC: btc, ETH: eth } = prices
  const isGreen = type === TxType.received  || type === TxType.buy
  switch (transport) {
    case Transport.Bitcoin:
      return (
        <Text isGreen={isGreen}>
          {satoshiToBtc(amount as number).toFixed(8)} BTC
        </Text>
      )
    case Transport.Ethereum:
      return (
        <Text isGreen={isGreen}>
          {weiToEth(amount as number).toFixed(8)} ETH
        </Text>
      )
    default:
      return (
        <Wrapper>
          <Text isGreen={isGreen}>{((amount as number) / btc).toFixed(8)} BTC</Text>
          <Text isGreen={isGreen}>{((amount as number) / eth).toFixed(8)} ETH</Text>
        </Wrapper>
      )
  }
}

export default Crypto