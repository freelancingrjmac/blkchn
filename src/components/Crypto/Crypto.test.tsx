/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react'
import { Provider } from 'react-redux'
import { render } from '@testing-library/react'

import Crypto from './Crypto'
import { Transport } from 'abstractions/transactions'

const prices = {
  ETH: 100,
  BTC: 100,
}

describe('Crypto', () => {
  it('renders', async () => {
    const a = render(
      <Crypto
        prices={prices}
        transport={Transport.Bitcoin}
        amount={899583795000000000}
      />,
    )
    expect(a).toMatchSnapshot()
  })
})
