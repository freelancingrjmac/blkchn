import React from 'react'
import { render } from '@testing-library/react'
import TestWrapper from '__test__/TestWrapper'

import App from './App'

test('renders', () => {
  const app = render(
    <TestWrapper>
      <App />
    </TestWrapper>,
  )
  expect(app).toMatchSnapshot()
})
