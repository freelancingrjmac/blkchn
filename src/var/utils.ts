import { SATOSHI_DENOMINATOR, WEI_DENOMINATOR } from './constants'

export const getTransactionDate = (timestamp: number): Date => {
  const date = new Date(0)
  date.setUTCSeconds(timestamp)
  return date
}

export const satoshiToBtc = (satoshis: number): number =>
  satoshis / SATOSHI_DENOMINATOR

export const weiToEth = (wei: number): number => wei / WEI_DENOMINATOR
