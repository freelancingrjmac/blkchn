import { combineReducers } from 'redux'
import transactionReducer from './transactions/transactions.reducer'
import pricesReducer from './prices/price.reducer'

const rootReducer = combineReducers({
  transactions: transactionReducer,
  prices: pricesReducer,
})

export type AppState = ReturnType<typeof rootReducer>

export default rootReducer
