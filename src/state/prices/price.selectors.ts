import { RootState } from 'abstractions'
import { Price } from 'abstractions/price'

export const getPrices = (state: RootState): Price => state.prices || {}
