import { ActionWithPayload } from 'abstractions'
import { Price } from 'abstractions/price'
import T from './price.types'

export const setPrices = (payload: Price): ActionWithPayload<Price> => ({
  type: T.CORE_SET_PRICES,
  payload,
})
