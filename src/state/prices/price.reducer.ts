import { ActionWithPayload } from 'abstractions'
import T from './price.types'
import { Price } from 'abstractions/price'
import { mergeRight } from 'ramda'

const initialState: Price = { BTC: 0, ETH: 0 }

export default (
  state = initialState,
  action: ActionWithPayload<Price>,
): Price => {
  const { type, payload } = action
  switch (type) {
    case T.CORE_SET_PRICES:
      return mergeRight(state, payload)
    default:
      return state
  }
}
