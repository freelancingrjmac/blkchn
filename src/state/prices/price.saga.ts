import { call, put } from 'redux-saga/effects'
import { SagaIterator } from 'redux-saga'
import { get } from 'api'
import { setPrices } from './price.actions'

export default function* priceSaga( ): SagaIterator {
  try {
    const prices = yield call(get, 'prices')
    yield put(setPrices(prices))
    return prices
  } catch (e) {
    console.log(e)
  }
}