import { spawn } from 'redux-saga/effects'
import { SagaIterator } from 'redux-saga'
import transactionSaga from './transactions/transaction.saga'
import pricesSaga from './prices/price.saga'

export default function* rootSaga(): SagaIterator {
  yield spawn(transactionSaga)
  yield spawn(pricesSaga)
}
