import { call, put } from 'redux-saga/effects'
import { SagaIterator } from 'redux-saga'
import { get } from 'api'
import * as A from './transaction.actions'
import {
  btcAdapter,
  custodialAdapter,
  ethAdapter,
} from './transaction.adapter'

export default function* transactionSaga(): SagaIterator {
  try {
    const btcTxs = yield call(get, 'btc-txs')
    yield put(A.setBtcTransactions(btcTxs))
    const ethTxs = yield call(get, 'eth-txs')
    yield put(A.setEthTransactions(ethTxs))
    const custTxs = yield call(get, 'custodial-txs')
    yield put(A.setCustodialTransactions(custTxs))
    const transactionItems = [
      ...btcAdapter(btcTxs),
      ...ethAdapter(ethTxs),
      ...custodialAdapter(custTxs),
    ]
    yield put(A.setAllTransactions(transactionItems))
  } catch (e) {
    console.log(e)
  }
}
