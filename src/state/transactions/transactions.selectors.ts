import { equals, filter, includes, where } from 'ramda'
import { RootState } from 'abstractions'

import {
  TransactionItem,
  Transactions,
  Transport,
} from 'abstractions/transactions'

export const getTransactions = (state: RootState): Transactions =>
  state.transactions || []

export const getAllTransactions = (state: RootState): TransactionItem[] =>
  getTransactions(state).all || []

export const getTransactionsByTransport = (transport: string) => (
  state: RootState,
): TransactionItem[] =>
  transport === Transport.All || !transport
    ? getAllTransactions(state)
    : filter<TransactionItem>(where({ transport: equals(transport) }))(
      getAllTransactions(state),
    )

export const getTransactionsByTerm = (term: string) => (
  state: RootState,
): TransactionItem[] =>
  filter<TransactionItem>(where({ regexp: includes(term) }))(
    getAllTransactions(state),
  )
