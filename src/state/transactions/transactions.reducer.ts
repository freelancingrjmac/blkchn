import { ActionWithPayload } from 'abstractions'
import { assoc } from 'ramda'
import {
  BtcTransaction,
  CustodialTransaction,
  EthTransaction,
  Transactions,
} from 'abstractions/transactions'
import T from './transactions.types'

const initialState: Transactions = {
  btc: [],
  eth: [],
  cust: [],
  all: [],
}

export default (
  state = initialState,
  action: ActionWithPayload<
    BtcTransaction[] | EthTransaction[] | CustodialTransaction[]
  >,
): Transactions => {
  const { type, payload } = action
  switch (type) {
    case T.CORE_SET_BTC_TRANSACTIONS:
      return assoc(
        'btc',
        payload,
        state,
      )
    case T.CORE_SET_ETH_TRANSACTIONS:
      return assoc(
        'eth',
        payload,
        state,
      )
    case T.CORE_SET_CUST_TRANSACTIONS:
      return assoc(
        'cust',
        payload,
        state,
      )
    case T.CORE_SET_ALL_TRANSACTIONS:
      return assoc('all', payload, state)
    default:
      return state
  }
}
