import {
  map,
  filter,
  join,
  toString,
  uniqBy,
  values,
  toLower,
  trim,
  isNil,
  flip,
  sortBy,
  Ord,
  reverse,
  compose,
  divide,
} from 'ramda'
import {
  AnyTransaction,
  BtcTransaction,
  CustodialTransaction,
  EthTransaction,
  TransactionItem,
  Transport,
} from 'abstractions/transactions'
import { getTransactionDate } from 'var/utils'


const toLowerString = compose(trim, toLower, toString)

const spacer = join(' ')

const toMts = (date: Date) => divide(date.getTime(), 1000)

const getTransactionString = (transaction: AnyTransaction) =>
  spacer(map(toLowerString, filter(flip(isNil), values(transaction))))

const btcTransaction = (transaction: BtcTransaction) => ({
  amount: transaction.amount,
  date: toMts(getTransactionDate(transaction.insertedAt)),
  from: transaction.from,
  id: transaction.hash,
  regexp: getTransactionString(transaction),
  status: transaction.state,
  to: transaction.to,
  transport: Transport.Bitcoin,
  type: transaction.type,
  amountCrypto: 0,
  currency: 'BTC',
  fiatCurrency: '',
})

const ethTransaction = (transaction: EthTransaction) => ({
  amount: transaction.amount,
  date: toMts(getTransactionDate(transaction.insertedAt)),
  from: transaction.from,
  id: transaction.hash,
  regexp: getTransactionString(transaction),
  status: transaction.state,
  to: transaction.to,
  transport: Transport.Ethereum,
  type: transaction.type,
  amountCrypto: 0,
  currency: 'ETH',
  fiatCurrency: '',
})

const custodialTransaction = (transaction: CustodialTransaction) => ({
  amount: transaction.fiatValue,
  date: toMts(new Date(transaction.createdAt)),
  from: 'Custodial',
  id: transaction.id,
  regexp: getTransactionString(transaction),
  status: transaction.state,
  to: 'Custodial',
  transport: Transport.Custodial,
  type: transaction.type,
  amountCrypto: 0,
  currency: '',
  fiatCurrency: '$',
})

export const btcAdapter = (transactions: BtcTransaction[]): TransactionItem[] =>
  uniqBy(({ id }) => id, map(btcTransaction, transactions))

export const ethAdapter = (transactions: EthTransaction[]): TransactionItem[] =>
  uniqBy(({ id }) => id, map(ethTransaction, transactions))

export const custodialAdapter = (
  transactions: CustodialTransaction[],
): TransactionItem[] =>
  uniqBy(({ id }) => id, map(custodialTransaction, transactions))

export const sortTransactionsByField = (
  key: Ord,
  transactions: TransactionItem[],
): TransactionItem[] =>
  reverse(sortBy((t: TransactionItem) => t[key.toString()], transactions))
