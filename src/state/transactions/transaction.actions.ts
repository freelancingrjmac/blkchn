import { ActionWithPayload } from 'abstractions'
import {
  BtcTransaction,
  CustodialTransaction,
  EthTransaction,
  TransactionItem,
} from 'abstractions/transactions'

import T from './transactions.types'

export const setAllTransactions = (
  payload: TransactionItem[],
): ActionWithPayload<TransactionItem[]> => ({
  type: T.CORE_SET_ALL_TRANSACTIONS,
  payload,
})

export const setBtcTransactions = (
  payload: BtcTransaction[],
): ActionWithPayload<BtcTransaction[]> => ({
  type: T.CORE_SET_BTC_TRANSACTIONS,
  payload,
})

export const setEthTransactions = (
  payload: EthTransaction[],
): ActionWithPayload<EthTransaction[]> => ({
  type: T.CORE_SET_ETH_TRANSACTIONS,
  payload,
})

export const setCustodialTransactions = (
  payload: CustodialTransaction[],
): ActionWithPayload<CustodialTransaction[]> => ({
  type: T.CORE_SET_CUST_TRANSACTIONS,
  payload,
})
