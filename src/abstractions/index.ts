import { Action } from 'redux'
import { Price } from './price'
import { Transactions } from './transactions'

export interface RootState {
  transactions: Transactions
  prices: Price
}
export interface ActionWithPayload<T> extends Action {
  payload: T
}
