import { Ord } from 'ramda'

export interface BtcTransaction {
  amount: number
  blockHeight: number
  coin: string
  description: string
  double_spend: boolean
  from: string
  fromWatchOnly: boolean
  hash: string
  insertedAt: number
  state: Status
  to: string
  toAddress: string
  toWatchOnly: boolean
  txFee: number
  type: TxType
  txfee?: number
  rbf?: boolean
}

export interface EthTransaction {
  amount: string | number
  blockHeight: string
  data: string
  description: string
  erc20: boolean
  from: string
  hash: string
  insertedAt: number
  state: Status
  to: string
  txFee: string
  type: TxType
}

export interface CustodialTransaction {
  createdAt: number
  id: string
  pair: string
  state: Status
  fiatValue: string
  fiatCurrency: string
  type: TxType
  version: string
}

interface ObjectKeys {
  [key: string]: Ord
}

export interface TransactionItem extends ObjectKeys {
  amount: string | number
  amountCrypto: string | number
  currency: string
  date: number
  fiatCurrency: string
  from: string
  status: Status
  regexp: string
  to: string
  type: TxType
  id: string
  transport: Transport
}

export interface Transactions {
  btc: BtcTransaction[]
  eth: EthTransaction[]
  cust: CustodialTransaction[]
  all: TransactionItem[]
}

export type AnyTransaction =
  | BtcTransaction
  | EthTransaction
  | CustodialTransaction

export enum TxType {
  buy = 'buy',
  sell = 'sell',
  sent = 'sent',
  received = 'received',
}

export enum Status {
  PENDING,
  COMPLETED,
}

export enum Transport {
  Bitcoin = 'Bitcoin',
  Ethereum = 'Ethereum',
  Custodial = 'Custodial',
  All = 'All',
}
