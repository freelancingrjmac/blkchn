## blockchain take home test

Install 

`yarn`

Development

`yarn start`

Test

`yarn test`

Build

`yarn build`

Considerations

- Improve sorting and filtering
- Would prefer to use react-virtual for table of large amount of data
- Over engineered on purpose to display different techniques
- Design could probably be improved
- Add web3 and some Bitcoin libraries to check validation of addresses and link to correct block explorer etc
- Add tests for redux module
- Add tests for utils